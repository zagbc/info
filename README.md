# Info
`Italian Version`

Ciao, questa è la repository dedicata alla Laurea Triennale in Ingegneria Informatica (Polimi) gestita dal PoliNetwork.
Ci troverete materiale riguardante i vari corsi e potrete caricarlo anche voi attraverso il bot telegram [@PoliMaterial_bot](https://t.me/PoliMaterial_bot)

- Primo anno https://gitlab.com/polinetwork/info1y
 
- Secondo anno https://gitlab.com/polinetwork/info2y
 
- Terzo anno https://gitlab.com/polinetwork/info3y

- Computer Science LM (Courses from A to E) https://gitlab.com/polinetwork/CompSc1

- Computer Science LM (Courses from F to Z) https://gitlab.com/polinetwork/CompSc2


`English Version`

Hello everyone this is the repository of BSc in Computer Engineering (Polimi) reserved by PoliNetwork.
You will be able to find and upload materials for your courses (Notes, Exercises, ...).
To upload files use the Telegram Bot [@PoliMaterial_bot](https://t.me/PoliMaterial_bot)

- First year https://gitlab.com/polinetwork/info1y
 
- Second year https://gitlab.com/polinetwork/info2y
 
- Third year https://gitlab.com/polinetwork/info3y

- Computer Science MD (Courses from A to E) https://gitlab.com/polinetwork/CompSc1

- Computer Science MD (Courses from F to Z) https://gitlab.com/polinetwork/CompSc2


`DISCLAIMER`
**You mustn't upload any copyright protected or in any other author protection form cover file on this repository or any of the ones associated to it.**

Polinetwork takes no responsibility for and we do not expressly or implicitly endorse, support, or guarantee the completeness, truthfulness, accuracy, or reliability of any of the content on this repository or any of the ones associated to it.
